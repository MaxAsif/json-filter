package me.asif;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.*;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;

@RestController
public class ApiController {
    @Autowired
    JdbcTemplate jdbc;

    @Autowired
    DataRepository dataRepository;
    @PostMapping(value = "/data")
    public Data create(@RequestBody Data data){
        return dataRepository.save(data);
    }


    @RequestMapping(value = "/endpoint", method = RequestMethod.POST,
            consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public List<Data> fun(@RequestBody MultiValueMap formParams){
        String data = (String) formParams.getFirst("data");
        addData(data);
        String rule_json = (String) formParams.getFirst("rule");
        JsonParser parser = new JsonParser();
        JsonArray jsonArray =  parser.parse(rule_json).getAsJsonArray();
        StringBuffer sb = new StringBuffer();
        sb.append("SELECT id,name,value FROM data WHERE ");
        int c = 0;
        for(JsonElement el:jsonArray) {
            c++;
            recursiveFun(el, sb,c, jsonArray.size());
        }
        System.out.println(sb);
        return(runSQL(sb.toString()));
    }
    public List<Data> runSQL(String sql){
        return jdbc.query(
                sql, new Object[] {},
                (rs, rowNum) -> new Data(rs.getInt("id"),rs.getString("name"), rs.getString("value"))
        );
    }
    public void recursiveFun(JsonElement jsonElement, StringBuffer sb, int c, int size){
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        if(jsonObject.get("condition").getClass().toString().contains("Object")) {
            sb.append(generateSql(jsonObject,c,size));
        }
        else{
            sb.append("(");
            int ctr = 0;
            for(JsonElement element:jsonObject.get("condition").getAsJsonArray()){
                ctr++;
                recursiveFun(element,sb,ctr,jsonObject.get("condition").getAsJsonArray().size());
            }
            sb.append(")");
        }
    }
    public String generateSql(JsonObject jsonObject, int c, int size){
        StringBuffer sql = new StringBuffer();
        ObjectMapper m = new ObjectMapper();
        try {
            Rule rule = m.readValue(jsonObject.toString(), Rule.class);
            System.out.println("\n\n \t\t\t Rule : "+rule+"\n\n");
            sql.append("(name = '" + rule.getCondition().getField() + "' and value ");
            sql.append(" " + operator(rule.getCondition().getOperator(), rule.getCondition().getFilterValue(), rule.getCondition().getFilterType()) + ") ");
            String str = jsonObject.get("logical_operator").toString();
           if(c<size)  sql.append(str.substring(1,str.length()-1)+" ");
        } catch (Exception e){
            e.printStackTrace();
        }
        return sql.toString();
    }
    private String operator(String operator, ArrayList<String> filterValue, String filterType) {
        if(operator.equals("equal"))
            return "= '"+ filterValue.get(0)+"' ";
        else if(operator.equals("not_equal"))
            return "!= '"+ filterValue.get(0)+"' ";
        else if(operator.equals("less"))
            return "< '"+ filterValue.get(0)+"' ";
        else if(operator.equals("less_or_equal"))
            return "<= '"+ filterValue.get(0)+"' ";
        else if(operator.equals("greater"))
            return "> '"+ filterValue.get(0)+"' ";
        else if(operator.equals("greater_or_equal"))
            return ">= '"+ filterValue.get(0)+"' ";
        else if(operator.equals("is_null"))
            return "IS NULL";
        else if(operator.equals("is_not_null"))
            return "IS NOT NULL";
        else if(filterType.equals("text") && operator.equals("begins_with"))
            return "LIKE '"+filterValue.get(0)+"%'";
        else if(filterType.equals("text") && operator.equals("not_begins_with"))
            return "NOT LIKE '"+filterValue.get(0)+"%'";
        else if(filterType.equals("text") && operator.equals("contains"))
            return "LIKE '%"+filterValue.get(0)+"%'";
        else if(filterType.equals("text") && operator.equals("not_contains"))
            return "NOT LIKE '%"+filterValue.get(0)+"%'";
        else if(filterType.equals("text") && operator.equals("ends_with"))
            return "LIKE '%"+filterValue.get(0)+"'";
        else if(filterType.equals("text") && operator.equals("not_ends_with"))
            return "NOT LIKE '%"+filterValue.get(0)+"'";
        else if(filterType.equals("text") && operator.equals("is_empty"))
            return "= ' '";
        else if(filterType.equals("text") && operator.equals("is_not_empty"))
            return "!= ' '";
        else if(filterType.equals("number") && operator.equals("in"))
            return "IN "+filterValue.toString().replace("[","(").replace("]",")");
        else if(filterType.equals("number") && operator.equals("not_in"))
            return "NOT IN "+ filterValue.toString().replace("[","(").replace("]",")");
        return "";
    }

    @GetMapping("/data")
    public List<Data> index(){
        return dataRepository.findAll();
    }

    @GetMapping("/data/{id}")
    public Data show(@PathVariable String id){
        System.out.println("sda\n");
        int dataId = Integer.parseInt(id);
        return dataRepository.findOne(dataId);
    }

    @GetMapping("/")
    public String getFirst(){
        return "hello world!";
    }
    public void addData(String data){
        JsonParser jsonParser = new JsonParser();
        JsonObject jsonObject = jsonParser.parse(data).getAsJsonObject();
        Set<Map.Entry<String, JsonElement>> entries = jsonObject.entrySet();
        for (Map.Entry<String, JsonElement> entry: entries) {
            System.out.println(entry.getKey()+" "+entry.getValue().getAsString()+" "+jsonObject.get(entry.getKey()).toString());
            dataRepository.save(new Data(entry.getKey(),entry.getValue().getAsString()));
        }
    }
}
