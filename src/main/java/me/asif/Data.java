package me.asif;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Data {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;
    private String value;

    public Data(){ }

    public Data(int id,String name,String value){
        this.setId(id);
        this.setName(name);
        this.setValue(value);
    }
    public Data(String name, String value){
        this.setName(name);
        this.setValue(value);
    }
    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }

    public void setName(String name ){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setValue(String value){
        this.value = value;
    }

    public String getValue(){
        return value;
    }

    @Override
    public String toString(){
        return "Data{"+
                "Name = "+ name +
                ", Value = " + value +
                "}";
    }
}
