package me.asif;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Condition {

//    public ArrayList<Rule> rules;
    private String filterType;
    public String numberType;
    private String field;
    private String operator;
    private ArrayList<String> filterValue;
    public Condition(){ }
    public Condition(String filterType, String numberType,String field, String operator, ArrayList<String> filterValue){
        this.setFilterType(filterType);
        this.setNumberType(numberType);
        this.setField(field);
        this.setOperator(operator);
        this.setFilterValue(filterValue);
    }

    public void setFilterType(String filterType){
        this.filterType = filterType;
    }
    public String getFilterType(){
        return filterType;
    }

    public void setNumberType(String numberType){
        this.numberType = numberType;
    }
    public void setField(String field){
        this.field = field;
    }
    public String getField(){
        return field;
    }
    public void setOperator(String operator){
        this.operator = operator;
    }
    public String getOperator(){
        return operator;
    }
    public void setFilterValue(ArrayList<String> filterValue){
        this.filterValue = new ArrayList<String>(filterValue);
    }
    public ArrayList<String> getFilterValue(){
        return filterValue;
    }

    @Override
    public String toString(){
        return " Condition {"+
                "filter Type = "+ filterType +
                ", field = " + field +
                ", operator = " + operator +
                ", filter Value = " + filterValue +
                "}";
    }
}
