package me.asif;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.TypeAdapter;

import java.util.ArrayList;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Rule {
    private String element_rule_id;
    private Condition condition;
    private String logical_operator;

    public Rule(){}
    public Rule(String element_rule_id, Condition condition, String logical_operator){
        this.setElementRuleId(element_rule_id);
        this.setCondition(condition);
        this.setLogicalOperator(logical_operator);
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public Condition getCondition(){
        return condition;
    }

    public void setLogicalOperator(String logical_operator) {
        this.logical_operator = logical_operator;
    }

    public String getLogicalOperator(){
        return logical_operator;
    }

    public void setElementRuleId(String element_rule_id) {
        this.element_rule_id = element_rule_id;
    }

    public String getElement_rule_id(){
        return element_rule_id;
    }

    @Override
    public String toString(){
        return "Rule {"+
                "element rule id = "+ element_rule_id +
                ", condition = " + condition +
                ", logical Operator = " + logical_operator +
                "}";
    }
}