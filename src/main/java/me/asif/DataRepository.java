package me.asif;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

public interface DataRepository extends  JpaRepository<Data, Integer>{
    @Query(value = "SELECT * FROM data where ?1", nativeQuery = true)
    public Object[][] customQuery(String str);
}
